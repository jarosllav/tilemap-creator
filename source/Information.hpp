#pragma once
#include <Windows.h>

#include <imgui.h>

class Information
{
public:
	Information()
	{

	}

	void Process()
	{
		if ( open != NULL ) {
			ImGui::Begin( "Information", &this->open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize );
			ImGui::NewLine();

			ImGui::Text( "[WSAD] - Move map" );
			ImGui::Text( "[LShift] - Speed move" );
			ImGui::Separator(); ImGui::NewLine();

			ImGui::Text( "[Left Mouse] - Set tile" );
			ImGui::Text( "[Right Mouse] - Set collision" );
			ImGui::Separator(); ImGui::NewLine();

			ImGui::BulletText( "Saving map" );
			ImGui::Text( "Type path to save file(with extends)\n and click \"Save\"" );

			ImGui::BulletText( "Loading map" );
			ImGui::Text( "Type path to load file(with extends)\n and click \"Load\"" );
			ImGui::Separator(); ImGui::NewLine();

			ImGui::BulletText( "Loading tileset" );
			ImGui::Text( "Type path to load tileset file\n(with extends) and click \"Load\"" );
			ImGui::TextColored( ImVec4( 1.f, 0.1f, 0.1f, 1.f ), "Remember! Avoid white signs\n in the path." );

			ImGui::End();
		}
	}

private:
	bool open;

};