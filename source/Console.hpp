#pragma once

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

#include <iostream>
#include <unordered_map>
#include <functional>

#include "Defines.hpp"
#include <imgui.h>

#include <memory>

class Console
{
public:
	Console() : MaxLines(10)
	{
		for ( int i = 0; i < MaxLines; ++i ) {
			lines.push_back( "" );
		}
		lastLine = 0;
	}

	void Process()
	{
		if ( CONSOLE_OPEN ) {
			ImGui::Begin( "Console" );
			for ( int i = 0; i < MaxLines; ++i ) {
				ImGui::Text( lines[i].c_str() );
			}
			ImGui::Text( "Input: " ); ImGui::SameLine();
			if ( ImGui::InputText( "##consoleInput", input, sizeof( input ), ImGuiInputTextFlags_EnterReturnsTrue ) ) {
				parseCmd();
				input[0] = 0;
			}

			ImGui::End();
		}
	}

	void Print( const std::string& text )
	{
		lines[lastLine] = text ;
		++lastLine;
		if ( lastLine >= lines.size() )
			lastLine = 0;
	}
private:
	void parseCmd()
	{
		std::string cmd = input;
		cmd = cmd.substr( 0, cmd.find( " " ) );

		this->Print( "Komenda nie istnieje! (" + cmd + ")");
	}

	const int MaxLines;
	int lastLine;
	std::vector<std::string> lines;

	char input[50];

};

static std::unique_ptr<Console> bConsole = std::make_unique<Console>();