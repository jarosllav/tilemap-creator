#pragma once

static const int TILE_SIZE = 64;
static int SELECTED_TILE = 0;

static bool DRAW_LINES = false;
static bool DRAW_COLLISION = false;
static bool CONSOLE_OPEN = false;
static bool TILESET_OPEN = false;

const sf::Vector2i WINDOW_SIZE = { 1080, 720 };