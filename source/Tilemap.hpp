#pragma once

#include <vector>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include "Defines.hpp"

struct Cell
{
	Cell( const sf::Vector2f& position, const sf::Texture& texture, const sf::IntRect& rect, int id )
	{
		this->pos = position;
		this->sprite.setPosition( this->pos );
		this->sprite.setTexture( texture );
		this->sprite.setTextureRect( rect );
		this->id = id;
	}

	sf::Sprite sprite;
	bool collision = false;
	sf::Vector2f pos;
	int id;
};

class Tilemap
{
public:
	Tilemap( const sf::Vector2i& size, const std::string& path ) : linesHorizontal(sf::Lines, 0), linesVertical(sf::Lines, 0)
	{
		this->tilesetPath = path;
		this->tileset.loadFromFile( path );
		this->mapSize = size;

		this->collisionShape.setFillColor( sf::Color( 255, 100, 50, 100 ) );
		this->collisionShape.setSize( sf::Vector2f( TILE_SIZE, TILE_SIZE ) );

		int x = tileset.getSize().x / TILE_SIZE;
		int y = tileset.getSize().y / TILE_SIZE;

		this->tilesetSize = sf::Vector2i( x, y );

		map.reserve( size.y );
		for ( int i = 0; i < size.y; ++i ) {
			std::vector<Cell> temp;
			for ( int j = 0; j < size.x; ++j ) {
				sf::IntRect rect( 0, 0, TILE_SIZE, TILE_SIZE );
				temp.push_back( Cell( sf::Vector2f( j * TILE_SIZE, i * TILE_SIZE ), tileset, rect, 0 ) );
			}
			map.push_back( temp );
		}

		horSize = WINDOW_SIZE.y / TILE_SIZE + 2;
		linesHorizontal.resize( horSize * 2 );
		vertSize = WINDOW_SIZE.x / TILE_SIZE + 2;
		linesVertical.resize( vertSize * 2 );

		for ( int i = 0; i < horSize; ++i ) {
			sf::Vertex* line = &linesHorizontal[i * 2];

			line[0].position = sf::Vector2f( 0, i * TILE_SIZE );
			line[0].color = sf::Color::Red;

			line[1].position = sf::Vector2f( WINDOW_SIZE.x, i * TILE_SIZE );
			line[1].color = sf::Color::Red;
		}
		for ( int i = 0; i < vertSize; ++i ) {
			sf::Vertex* line = &linesVertical[i * 2];

			line[0].position = sf::Vector2f( i * TILE_SIZE, 0 );
			line[0].color = sf::Color::Red;

			line[1].position = sf::Vector2f( i * TILE_SIZE, WINDOW_SIZE.y );
			line[1].color = sf::Color::Red;
		}
	}

	void Draw( sf::RenderTarget& target, sf::RenderStates states )
	{
		for ( auto& cells : this->map ) {
			for ( auto& cell : cells ) {
				target.draw( cell.sprite, states );

				if ( DRAW_COLLISION ) {
					if ( cell.collision ) {
						collisionShape.setPosition( cell.pos );
						target.draw( collisionShape, states );
					}
				}
			}
		}

		if ( DRAW_LINES ) {
			target.draw( linesHorizontal, states );
			target.draw( linesVertical, states );
		}
	}

	Cell* GetCell( sf::Vector2f position )
	{
		for ( auto& cells : this->map )
			for(auto& cell : cells)
				if ( position == cell.pos )
					return &cell;

		return nullptr;
	}
	bool IsCellInside( Cell* cell, sf::Vector2f position )
	{
		sf::Vector2f size = { cell->sprite.getGlobalBounds().height, cell->sprite.getGlobalBounds().width };
		sf::Vector2f pos = cell->pos;
		if ( position.x > pos.x && position.x < pos.x + size.y &&
			 position.y > pos.y && position.y < pos.y + size.x )
			return true;
		return false;
	}
	void SetCell( sf::Vector2f position , int id)
	{
		for ( auto& cells : this->map ) {
			for ( auto& cell : cells ) {
				if ( IsCellInside( &cell, position ) ) {
					sf::IntRect rect( id * TILE_SIZE, 0, TILE_SIZE, TILE_SIZE );
					cell.sprite.setTexture( this->tileset );
					cell.sprite.setTextureRect( rect );
					cell.id = id;
					break;
				}
			}
		}
	}
	void SetCell( sf::Vector2f position , bool col)
	{
		for ( auto& cells : this->map ) {
			for ( auto& cell : cells ) {
				if ( IsCellInside( &cell, position ) ) {
					cell.collision = col;
					break;
				}
			}
		}
	}

	inline void SetCells( const std::vector<std::vector<Cell>>& cells ) { this->map.clear(); this->map = cells; }
	inline std::vector<std::vector<Cell>> GetCells() { return this->map; }
	inline sf::Vector2i GetMapSize() { return this->mapSize; }
	inline void SetMapSize( float x, float y ) { this->mapSize = sf::Vector2i( x, y ); }
	inline sf::Texture& GetTileset() { return this->tileset; }
	inline std::string GetTilesetPath() { return this->tilesetPath; }

	////
	inline void SetTileset( const sf::Texture& texture, const std::string& path ) { 
		this->tileset = texture; 
		this->tilesetPath = path;
		int x = tileset.getSize().x / TILE_SIZE;
		int y = tileset.getSize().y / TILE_SIZE;

		this->tilesetSize = sf::Vector2i( x, y );
	}
	void ReloadCells()
	{
		for ( auto& cells : this->map ) {
			for ( auto& cell : cells ) {
				sf::IntRect rect( cell.id * TILE_SIZE, 0, TILE_SIZE, TILE_SIZE );
				cell.sprite.setTexture( this->tileset );
				cell.sprite.setTextureRect( rect );
			}
		}
	}
private:
	std::vector<std::vector<Cell>> map;
	sf::RectangleShape collisionShape;

	sf::VertexArray linesVertical; int vertSize = 0; 
	sf::VertexArray linesHorizontal; int horSize = 0;
	
	sf::Texture tileset;
	std::string tilesetPath;

	sf::Vector2i mapSize;
	sf::Vector2i tilesetSize;
};